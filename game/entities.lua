local entities = {
  
  load = function(file)
    
    local object = lf.load(("resources/entities/%s.ent"):format(file))()
    
    object.state    = false
    object.pos      = {0,0}
    object.show     = false
    
    if object.anim then
      
      object.anim = assets.animation(object.anim.name,object.anim.data)
      
      function object:draw(x,y)
        
        x,y = (x*32)-(16),(y*32)-(16)
        
        if game.state.selected == self then
          camera.fx(
            function(cx,cy,cr,cz)
              
              lg.setColor(100,100,255,150)
              lg.rectangle("fill",x+cx-16,y+cy-16,32,32)
              
            end,
            1,"B"
          )
          
        end
        
        self.anim:draw(x,y,0,1,1,4)
        
      end
      
      function object:update(dt)
        
        self.anim:update(dt)
        
      end
      
    else
      
      function object:draw(x,y)
        
        local cx,cy,cr,cz = unpack(camera.curr)
        x,y = (x*32)-(16-cz),(y*32)-(16-cz)
        local color = {255,255,255,255}
        
        if game.state.selected == self then
          camera.fx(
            function(cx,cy,cr,cz)
              
              lg.setColor(100,100,255,150)
              lg.rectangle("fill",x+cx-16,y+cy-16,32,32)
              
            end,
            1,"B"
          )
          
        end
        
        camera.draw(assets.image(self.sprite),x,y,0,1,1, "image", 4, color)
        
      end
      
      function object:update()
        
        
        
      end
      
      
    end
    
    function object:showcase()
      
      local drawfunc = function()
        
        local midx = (win.w/2)-360
        local midy = (win.h/2)-120
        local a = {#self.name,#self.race,#self.class}
        table.sort(a)
        
        assets.font("copperplate gothic bold")
        lg.setColor(0,0,0,200)
        lg.rectangle("fill",midx+3,midy+3,353,353)
        lg.setColor(40,40,50,255)
        lg.rectangle("fill",midx,midy,350,350)
        lg.setColor(255,255,255,255)
        if self.anim then
          local img,quad = assets.texture(self.anim:getStage())
          lg.draw(img,quad,midx,midy,0,math.resize(32,32,350,350))
        else
          lg.draw(assets.image(self.sprite),midx,midy,0,math.resize(32,32,350,350))
        end
        lg.setColor(20,20,20,235)
        lg.rectangle("fill",midx+360,midy,12*a[3],52)
        lg.setColor(235,235,235,255)
        lg.print(self.name,midx+368,midy)
        lg.print(self.race,midx+368,midy+16)
        lg.print(self.class,midx+368,midy+32)
        
      end
      
      camera.gui(drawfunc , 5, "B")
      
    end
    
    return object
    
  end
  
}

return entities