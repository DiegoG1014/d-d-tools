function perc(a,b)
  return a*(b/100)
end

function percOf(n,per)
  return (n*100)/per
end

local cur,max,w
cur = 10
max = 255
w   = 250

print(
  perc(
    percOf(
      cur,
      max
    ),
    w
  )
)
print(percOf(cur,max))